import re
import random
from typing import List

from telebot import TeleBot, logger, logging
from telebot.handler_backends import BaseMiddleware, CancelUpdate
from telebot.types import (
    BotCommand,
    Message,
    InlineQuery,
    InlineQueryResultArticle,
    InputLocationMessageContent,
)
from telebot.util import update_types

from sqlalchemy import create_engine, asc, and_, text
from sqlalchemy.orm import scoped_session, sessionmaker, Session

from config import CONFIG
from keys import UNIQUE_KEYWORDS, UNIQUE_TYPES
from model import *

logger.setLevel(logging.INFO)

engine = create_engine(
    "mysql+pymysql://{}:{}@{}:{}/{}".format(
        CONFIG.DB_USER,
        CONFIG.DB_PASSWORD,
        CONFIG.DB_HOST,
        CONFIG.DB_PORT,
        CONFIG.DB_NAME,
    )
)

Scoped_session = scoped_session(sessionmaker(bind=engine))


name_mapping = {
    "ключевое слово": {"table": Keywords, "column": "keyword", "textcol": "keywords"},
    "тип": {"table": Types, "column": "type", "textcol": "types"},
    "название": {"table": Names, "column": "place_name", "textcol": "names"},
}

numresults_storage = dict()


class AntiRepeatMiddleware(BaseMiddleware):
    def __init__(self, seconds_limit: float = 0.8):
        self.update_types = ["message"]
        self.last_datetime = {}
        self.last_text = {}
        self.seconds_limit = seconds_limit
    
    def pre_process(self, message, data):
        if not message.from_user.id in self.last_datetime:
            self.last_datetime[message.from_user.id] = message.date
            self.last_text[message.from_user.id] = message.text
        else:
            date_diff = message.date - self.last_datetime[message.from_user.id]
            text_diff = message.text == self.last_text[message.from_user.id]
            seconds_diff = date_diff.total_seconds()
            if seconds_diff <= self.seconds_limit and text_diff:
                return CancelUpdate()

    def post_process(self, message, data, exception):
        pass


class SessionMiddleware(BaseMiddleware):
    def __init__(self):
        self.update_types = update_types

    def pre_process(self, message, data):
        data["session"] = Scoped_session()

    def post_process(self, message, data, exception):
        if exception:
            logger.info(exception)
        Scoped_session.remove()


bot: TeleBot = TeleBot(CONFIG.BOT_TOKEN, use_class_middlewares=True)

bot.delete_my_commands(scope=None, language_code=None)

bot.set_my_commands(
    commands=[
        BotCommand("start", "Запустить бота."),
        BotCommand("help", "Показать доступные комманды."),
        BotCommand("random", "Случайный объект."),
        BotCommand("types", "Список типов объектов"),
        BotCommand("keywords", "Список ключевых слов")
    ],
)


def query_in_list(query: InlineQuery) -> bool:
    if not query.query:
        return False
    search = re.search(r"([кК]лючевое [сС]лово|[тТ]ип|[нН]азвание) [А-Яа-я]{3,}", query.query)
    if not search:
        return False
    return True


def compose_message(row: Texts) -> list:
    header = ", ".join([str(item) for item in row.names]) + "\n\n"
    body = header + str(row)
    if len(body) > 4096:
        body = re.match(r".{1,4093}\.", body).group()
    keywords = "<b>Ключевые слова: </b>" + " ".join(
        [str(item) for item in row.keywords]
    )
    types = "<b>Типы: </b>" + " ".join([str(item) for item in row.types])
    footer = "\n".join([keywords, types])
    if (
        len(body) + len(footer) < 4094
    ):  # when the message does not exceed Telegram's limits, send info and metadata together
        return ["\n".join([body, footer])]
    return [body, footer]


@bot.message_handler(commands=["start", "help"])
def help_message(message: Message, data: dict):
    help_text = """
    - Отправьте команды /start или /help, чтобы узнать о возможностях бота.
    - Отправьте локацию (доступно в мобильной версии Telegram), чтобы найти ближайшую точку на фольклорной карте и её описание.
    - Нажмите на полученную от бота локацию, чтобы построить маршрут
    Команды для поиска объектов:
        - Для выборки по ключевому слову (Сталин): @folk_map_bot ключевое слово Сталин
        - Для выборки по типу (Здание): @folk_map_bot тип Здание
        - Для выборки по названию (Дом): @folk_map_bot название Дом на набережной
    Чтобы выполнить любую из 3 комманд выше, вы можете позвать бота в любом чате.
    """
    bot.send_message(message.from_user.id, help_text, parse_mode="html")


@bot.message_handler(commands=["random"])
def random_object(message: Message, data: dict):
    session: Session = data["session"]

    query = session.query(Texts).filter(
        and_(Texts.longitude != None, Texts.latitude != None)
    )

    results: List[Texts] = query.all()

    random_result = random.choice(results)
    for message_part in compose_message(random_result):
        bot.send_message(message.from_user.id, message_part, parse_mode="html")


@bot.message_handler(commands=["types"])
def send_types(message: Message, data: dict):
    new_message = "<b>Типы: </b>" + "\n" + "\n".join(["#" + str(item) for item in UNIQUE_TYPES])
    bot.send_message(message.from_user.id, new_message, parse_mode="html")


@bot.message_handler(commands=["keywords"])
def send_keywords(message: Message, data: dict):
    new_message = "<b>Ключевые слова: </b>" + "\n" + "\n".join(["#" + str(item) for item in UNIQUE_KEYWORDS])
    bot.send_message(message.from_user.id, new_message, parse_mode="html")


@bot.inline_handler(func=query_in_list)
def get_by_key(inline_query: InlineQuery, data: dict):
    session: Session = data["session"]
    search = re.search(
        r"([кК]лючевое [сС]лово|[тТ]ип|[нН]азвание) [А-Яа-я]{3,}", inline_query.query
    ).group()
    query_type, query_word = search.split(" ", 1)
    query_type = query_type.lower()
    if not re.match(r"[А-Я][а-я]+", query_word):
        query_word = query_word.capitalize()

    table, column, textcol = (
        name_mapping[query_type]["table"],
        name_mapping[query_type]["column"],
        name_mapping[query_type]["textcol"],
    )
    logger.info(f"got query {query_word} for table: {table.__name__}")

    # get column values in the target table that start with the input substring
    list_query = (
        session.query(table).filter(getattr(table, column).startswith(query_word)).all()
    )

    if not list_query:
        logger.info(f"substring {query_word} not found in {table.__name__}, exiting")
        return

    vars_list = [str(getattr(item, column)) for item in list_query]
    if query_word not in vars_list:
        logger.info(
            f"substring {query_word} not found in {table.__name__}, returning options"
        )
        result_query = session.query(Texts).filter(
            and_(
                getattr(Texts, textcol).any(getattr(table, column).in_(vars_list)),
                Texts.longitude != None,
                Texts.latitude != None,
            )
        )
    else:
        logger.info(f"substring {query_word} found, returning exact matches")
        result_query = session.query(Texts).filter(
            and_(
                getattr(Texts, textcol).any(getattr(table, column) == query_word),
                Texts.longitude != None,
                Texts.latitude != None,
            )
        )

    results = result_query.all()
    if not results:
        logger.info(f"No texts found for {column} {query_word}, exiting function")
        return

    results = results[:50]

    query_answer = [
        InlineQueryResultArticle(
            str(idx + 1),
            namelist[0].place_name
            if len(namelist := item.names) > 0
            else f"Объект {item.id}",
            InputLocationMessageContent(
                latitude=float(item.latitude), longitude=float(item.longitude)
            ),
        )
        for idx, item in enumerate(results)
    ]
    bot.answer_inline_query(inline_query.id, query_answer)


@bot.inline_handler(
    func=lambda query: query.query is not None and re.search(r"id [0-9]+", query.query)
)
def get_ids(inline_query: InlineQuery, data: dict):
    session: Session = data["session"]
    query_id = re.findall(r"[0-9]+", inline_query.query)[-1]

    query = session.query(Texts).filter(
        and_(
            Texts.id.startswith(query_id),
            Texts.longitude != None,
            Texts.latitude != None,
        )
    )

    results: List[Texts] = query.all()[:50]

    query_answer = [
        InlineQueryResultArticle(
            str(idx + 1),
            namelist[0].place_name
            if len(namelist := item.names) > 0
            else f"Объект {item.id}",
            InputLocationMessageContent(
                latitude=float(item.latitude), longitude=float(item.longitude)
            ),
        )
        for idx, item in enumerate(results)
    ]
    bot.answer_inline_query(inline_query.id, query_answer)


@bot.inline_handler(func=lambda query: query.location is not None)
@bot.message_handler(
    func=lambda msg: msg.location is not None, content_types=["location"]
)
def nearest_location(message: Message, data: dict):
    session: Session = data.get("session")
    latitude, longitude = message.location.latitude, message.location.longitude

    logger.info(f"Got latitude: {latitude}, longitude: {longitude}")
    matching_tolerance = (
        0.000050  # roughly equivalent to 50 meters in terms of latitude
    )
    exact_query = session.query(Texts).filter(
        and_(
            text(f"ABS(texts.latitude - {latitude}) < {matching_tolerance}"),
            text(f"ABS(texts.longitude - {longitude}) < {matching_tolerance}"),
        )
    )
    # logger.info(str(exact_query.statement))
    exact_result = exact_query.first()
    if exact_result:
        for message_part in compose_message(exact_result):
            bot.send_message(message.from_user.id, message_part, parse_mode="html")
        return

    # logger.info(f"exact result not found, looking for nearest")
    query = (
        session.query(Texts)
        .filter(and_(Texts.longitude != None, Texts.latitude != None))
        .order_by(
            asc(
                (Texts.longitude - longitude) * (Texts.longitude - longitude)
                + (Texts.latitude - latitude) * (Texts.latitude - latitude)
            )
        )
    )
    result: Texts = query.first()

    if not result:
        logger.info("no results, exiting function")
        return

    new_latitude, new_longitude = result.latitude, result.longitude
    # logger.info(f"sending coords: lat: {new_latitude}, long: {new_longitude}")
    # logger.info(f"float values: lat: {float(new_latitude)}, long: {float(new_longitude)}")

    bot.send_location(
        message.from_user.id, latitude=new_latitude, longitude=new_longitude
    )
    for message_part in compose_message(result):
        bot.send_message(message.from_user.id, message_part, parse_mode="html")


@bot.inline_handler(func=lambda query: True)
def get_ids(inline_query: InlineQuery, data: dict):
    session: Session = data["session"]

    query = session.query(Texts).filter(
        and_(Texts.longitude != None, Texts.latitude != None)
    )

    results: List[Texts] = query.all()[:50]

    query_answer = [
        InlineQueryResultArticle(
            str(idx + 1),
            namelist[0].place_name
            if len(namelist := item.names) > 0
            else f"Объект {item.id}",
            InputLocationMessageContent(
                latitude=float(item.latitude), longitude=float(item.longitude)
            ),
        )
        for idx, item in enumerate(results)
    ]
    bot.answer_inline_query(inline_query.id, query_answer)


bot.setup_middleware(AntiRepeatMiddleware())
bot.setup_middleware(SessionMiddleware())

if __name__ == "__main__":
    bot.infinity_polling()
