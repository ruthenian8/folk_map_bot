import re


def htmize(substr: str) -> str:
    new_substr = (
        substr.replace("<", "&lt;")
        .replace(">", "&gt;")
        .replace("&", "&amp;")
        .replace('"', "&quot;")
    )
    return new_substr


def escape(substr: str) -> str:
    match: re.Match
    new_substr = re.sub(
        r"[_*\[\]()~`>#+-=|{}.!]", lambda match: "\\" + match.group(), substr
    )
    return new_substr
