from sqlalchemy import UniqueConstraint, Column, Table, MetaData, ForeignKey, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.dialects.mysql import DOUBLE, INTEGER, TEXT, VARCHAR, TINYTEXT, DECIMAL

from local_utils import htmize

metadata = MetaData(quote_schema=False)

Base = declarative_base(metadata=metadata)


class Names(Base):
    __tablename__ = "place_names"
    id = Column("id", INTEGER, primary_key=True)
    text_id = Column("text_id", ForeignKey("texts.id", ondelete="CASCADE"))
    place_name = Column("place_name", TINYTEXT)

    def __repr__(self):
        return f"<b>{htmize(self.place_name)}</b>"


Text2Keyword = Table(
    "text2keyword",
    Base.metadata,
    Column("id", INTEGER, primary_key=True),
    Column("id_text", ForeignKey("texts.id", ondelete="CASCADE")),
    Column("id_keyword", ForeignKey("keywords.id", ondelete="CASCADE")),
)

Text2Type = Table(
    "text2type",
    Base.metadata,
    Column("id", INTEGER, primary_key=True),
    Column("id_text", ForeignKey("texts.id", ondelete="CASCADE")),
    Column("id_type", ForeignKey("types.id", ondelete="CASCADE")),
)


class Keywords(Base):
    __tablename__ = "keywords"
    id = Column("id", INTEGER, primary_key=True)
    keyword = Column("keyword", VARCHAR(128), nullable=False, unique=True)

    def __repr__(self):
        return f'#{self.keyword.replace(" ", "_")}'


class Types(Base):
    __tablename__ = "types"
    id = Column("id", INTEGER, primary_key=True)
    type = Column("type", VARCHAR(128), nullable=False, unique=True)

    def __repr__(self):
        return f"#{self.type}"


class Texts(Base):
    __tablename__ = "texts"
    id = Column("id", INTEGER, primary_key=True)
    address = Column("address", TINYTEXT, server_default=text("NULL"))
    longitude = Column(
        "longitude", DECIMAL(precision=8, scale=6), server_default=text("NULL")
    )
    latitude = Column(
        "latitude", DECIMAL(precision=8, scale=6), server_default=text("NULL")
    )
    sujet = Column("sujet", TEXT, server_default=text("NULL"))
    practica = Column("practica", TEXT, server_default=text("NULL"))
    names = relationship("Names")
    keywords = relationship("Keywords", secondary=Text2Keyword)
    types = relationship("Types", secondary=Text2Type)

    def __repr__(self):
        return (
            ("<b>Сюжеты: </b>" + self.sujet + "\n")
            if self.sujet
            else "" + ("<b>Практики: </b>" + self.practica + "\n")
            if self.practica
            else ""
        )


__all__ = [
    "Texts",
    "Text2Keyword",
    "Text2Type",
    "Names",
    "Types",
    "Keywords",
]
