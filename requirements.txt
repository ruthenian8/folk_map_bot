Flask<=2.1.1
greenlet<=1.1.2
pyTelegramBotAPI  @ git+https://github.com/eternnoir/pyTelegramBotAPI.git@master
requests<=2.27.1
SQLAlchemy<=1.4.35
pymysql<=1.0.2