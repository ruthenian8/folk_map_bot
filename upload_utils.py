import pandas as pd

from sqlalchemy.engine import Engine

from config import CONFIG


class JoinedTable(object):
    def __init__(self, tablename: str, engine: Engine):
        self.engine: Engine = engine
        self.tablename: str = tablename
        self.dataframe: pd.DataFrame = pd.read_sql_table(
            self.tablename, schema=CONFIG.DB_NAME, con=self.engine
        )
        if self.dataframe.shape[0] > 0:
            self.last_index = self.dataframe["id"].iloc[-1]
        else:
            self.last_index = 0

    @property
    def df(self) -> pd.DataFrame:
        return self.dataframe

    def drop_duplicates(self, *args, **kwargs):
        kwargs["inplace"] = kwargs.get("inplace") or False
        kwargs["subset"] = kwargs.get("subset") or [
            col for col in self.dataframe.columns if "id" not in col
        ]
        if not kwargs["inplace"]:
            self.dataframe = self.dataframe.drop_duplicates(*args, **kwargs)
            return self
        self.dataframe.drop_duplicates(*args, **kwargs)

    def join(self, new_dataframe: pd.DataFrame):
        if not "id" in new_dataframe.columns:
            new_dataframe.insert(
                loc=0,
                column="id",
                value=list(
                    range(
                        self.last_index + 1,
                        self.last_index + 1 + new_dataframe.shape[0],
                    )
                ),
            )
        self.dataframe = self.dataframe.append(new_dataframe)
        return self

    @property
    def new_indices(self) -> pd.Series:
        return self.dataframe.loc[self.dataframe["id"] > self.last_index]["id"]

    @property
    def new_entries(self) -> pd.DataFrame:
        return self.dataframe.iloc[-self.new_indices.shape[0] :, :]

    def upload(self):
        if self.new_indices.shape[0] == 0:
            print(f"nothing to insert in {self.tablename}")
            return
        self.new_entries.to_sql(
            name=self.tablename,
            con=self.engine,
            schema=CONFIG.DB_NAME,
            index=False,
            if_exists="append",
        )
        print(f"upload to {self.tablename} complete")


class JoinedRelation(JoinedTable):
    def __init__(
        self,
        tablename: str,
        engine: Engine,
    ):
        super().__init__(tablename, engine)

    def join(
        self,
        main_dataframe: pd.DataFrame,
        relation_dataframe: pd.DataFrame,
        on: tuple = ("value", "code"),
        suffixes: tuple = ("_x", "_y"),
    ):
        new_dataframe = pd.merge(
            left=main_dataframe,
            right=relation_dataframe,
            suffixes=suffixes,
            left_on=[on[0]],
            right_on=[on[1]],
            # how="left"
        )
        id_cols = ["id" + suffixes[0], "id" + suffixes[1]]
        new_dataframe = new_dataframe[id_cols]
        return super().join(new_dataframe)
