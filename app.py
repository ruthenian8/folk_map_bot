from flask import Flask, request, abort
from telebot import types

from bot import bot
from config import CONFIG

application = Flask(__name__)

HOOK_URL_BASE = "{}://{}{}".format(
    CONFIG.PROTOCOL, CONFIG.HOST, (":" + CONFIG.PORT) if CONFIG.PORT else ""
)

HOOK_URL_ENDPOINT = f"/webhook/"


@application.route("/diagnostics", methods=["GET"])
def diagnostics():
    return "ok"


@application.route(HOOK_URL_ENDPOINT, methods=["POST"])
def bot_hook():
    if request.headers.get("content-type") == "application/json":
        json_string = request.get_data().decode("utf-8")
        update = types.Update.de_json(json_string)
        bot.process_new_updates([update])
        return ""
    else:
        abort(403)

bot.remove_webhook()
bot.set_webhook(url=HOOK_URL_BASE + HOOK_URL_ENDPOINT)


if __name__ == "__main__":
    application.run(host="0.0.0.0")
