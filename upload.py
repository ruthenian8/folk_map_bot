import sys
import csv
from typing import Optional
import warnings

warnings.simplefilter(action="ignore", category=FutureWarning)

import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine

from model import *
from upload_utils import JoinedTable, JoinedRelation
from config import FIELDNAMES, CONFIG


def expand_and_melt(df: pd.DataFrame, target_col: str):
    """
    Turn a comma-separated column into a long-format mapping
    """
    wide = df[target_col].str.split(", ", expand=True)
    wide_concat = pd.concat([df[["id"]], wide], axis=1)
    long = (
        pd.melt(wide_concat, id_vars=["id"]).dropna(axis=0).drop(["variable"], axis=1)
    )
    return long


def main(filename: str, upload_uniques: bool = False):
    engine = create_engine(
        "mysql+pymysql://{}:{}@{}:{}/{}".format(
            CONFIG.DB_USER,
            CONFIG.DB_PASSWORD,
            CONFIG.DB_HOST,
            CONFIG.DB_PORT,
            CONFIG.DB_NAME,
        )
    )
    main_table = pd.read_csv(filename, sep="\t", quotechar='"')
    main_table.columns = FIELDNAMES
    main_table[["longitude", "latitude"]] = main_table.coords.str.split(
        ", ", expand=True
    )
    texts_table = JoinedTable("texts", engine)
    texts_table.join(main_table[texts_table.dataframe.columns])
    # texts_table.upload()

    names = (
        pd.melt(main_table[["id", "name", "unoff"]], id_vars=["id"])
        .dropna(axis=0)
        .drop(["variable"], axis=1)
        .rename(columns={"id": "text_id", "value": "name"})
    )
    names_table = JoinedTable("names", engine).join(names)
    # names_table.upload()

    long_keys = expand_and_melt(main_table, "keywords")
    long_keys.value = long_keys.value.apply(lambda x: x.strip().title())
    long_types = expand_and_melt(main_table, "types")
    long_types.value = long_types.value.apply(lambda x: x.strip().title())

    keywords_table = JoinedTable(tablename="keywords", engine=engine)
    types_table = JoinedTable(tablename="types", engine=engine)

    if upload_uniques:
        keywords_table.join(
            pd.DataFrame.from_dict({"keyword": pd.unique(long_keys.value)})
        )
        types_table.join(pd.DataFrame.from_dict({"type": pd.unique(long_types.value)}))
        # print(keywords_table.new_entries.tail())
        keywords_table.upload()
        types_table.upload()

    text2keyword_table = JoinedRelation("text2keyword", engine)
    text2type_table = JoinedRelation("text2type", engine)

    text2keyword_table.join(
        long_keys,
        keywords_table.dataframe,
        on=("value", "keyword"),
        suffixes=("_text", "_keyword"),
    )
    text2type_table.join(
        long_types,
        types_table.dataframe,
        on=("value", "type"),
        suffixes=("_text", "_type"),
    )
    # text2keyword_table.upload()
    # text2type_table.upload()


if __name__ == "__main__":
    assert len(sys.argv) > 1, "Required arguments: filename"

    main(sys.argv[1])
